FROM php:7.4-apache-buster

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

ENV DEBIAN_FRONTEND=noninteractive

RUN sed -i -e 's/ main/ main contrib non-free/g' /etc/apt/sources.list \
    && apt-get -qq update && apt-get -qq -y --no-install-recommends install \
      snmp fping mariadb-client python-mysqldb rrdtool whois mtr-tiny \
      ipmitool graphviz imagemagick curl libpng-dev libjpeg-dev libfreetype6-dev \
      libvirt-clients snmp-mibs-downloader gettext-base cron sudo \
    && apt-get clean && rm -rf /var/lib/apt/lists

RUN docker-php-ext-configure gd -with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) \
       mysqli pdo_mysql

WORKDIR /opt

RUN curl -s -L -o observium.tgz http://www.observium.org/observium-community-latest.tar.gz \
    && tar -xzf observium.tgz \
    && rm observium.tgz

WORKDIR /opt/observium

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY cron /opt/observium/cronfile.subst
COPY snmp.conf /etc/snmp/snmp.conf

RUN for f in /usr/bin/fping /usr/bin/mtr /usr/bin/mtr-packet; \
    do setcap CAP_NET_RAW+ep $f; \
    done

RUN a2enmod rewrite

RUN mkdir -p logs \
    && mkdir -p --mode=u+rwx,g+rs,o-w rrd \
    && useradd -G www-data observium \
    && chown -R observium:observium logs \
    && chown -R observium:www-data rrd \
    && chmod -R g+w rrd
